package main;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Record implements Serializable {

    transient private String firstName;
    transient private String secondName;
    transient private String surname;
    private Long number;
    private String fullName;
    private String phoneNumber;
    private String address;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Record{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", surname='" + surname + '\'' +
                ", number=" + number +
                ", fullName='" + fullName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Record record = (Record) o;

        if (fullName != null ? !fullName.equals(record.fullName) : record.fullName != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(record.phoneNumber) : record.phoneNumber != null) return false;
        return address != null ? address.equals(record.address) : record.address == null;

    }

    @Override
    public int hashCode() {
        int result = fullName != null ? fullName.hashCode() : 0;
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        return result;
    }

    public void createFullName() {
        StringBuffer stringBuffer = new StringBuffer();
        if (surname != null) stringBuffer.append(surname).append(" ");
        if (firstName != null) stringBuffer.append(firstName).append(" ");
        if (secondName != null) stringBuffer.append(secondName);
        fullName = stringBuffer.toString();
    }

    private void setFields() {
        String[] name = this.fullName.split(" ");
        surname = name[0];
        firstName = name[1];
        secondName = name[2];
    }

    private void writeObject(ObjectOutputStream outputStream) throws IOException {
        if (fullName==null) createFullName();
        outputStream.defaultWriteObject();
    }

    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        inputStream.defaultReadObject();
        setFields();
    }

}
