package main;

import java.util.ArrayList;
import java.util.List;

public class PhoneBook {

    private List<Record> records = new ArrayList<>();

    @Override
    public String toString() {
        return "PhoneBook{" +
                "records=" + records +
                '}';
    }

    public void addRecord(Record record) {
        records.add(record);
        record.setNumber((long) (records.size() + 1));
    }

    public void removeRecord(Record record) {
        records.remove(record);
        for (Record rec : records) {
            if (rec.getNumber() > record.getNumber()) rec.setNumber(rec.getNumber() - 1);
        }
    }

    public boolean contains(Record record) {
        if (records.contains(record)) return true;
        return false;
    }

    public Record findByNumber(Long number) {
        for (Record rec : records) {
            if (rec.getNumber().equals(number)) return rec;
        }
        return null;
    }

    public List<Record> findByFullName(String name) {
        List<Record> findRecords = new ArrayList<>();
        if (name == null) {
            System.out.println("empty name");
        } else
            for (Record rec : records) {
                if (rec.getFullName().contains(name.trim())) findRecords.add(rec);
            }
        return findRecords;
    }

    public List<Record> findByPhoneNumber(String phoneNumber) {
        List<Record> findRecords = new ArrayList<>();
        if (phoneNumber == null) {
            System.out.println("empty Phone");
        } else
            for (Record rec : records) {
                if (rec.getNumber().equals(phoneNumber)) findRecords.add(rec);
            }
        return findRecords;
    }

    public List<Record> findByAddress(String address) {
        List<Record> findRecords = new ArrayList<>();
        if (address == null) {
            System.out.println("empty address");
        } else
            for (Record rec : records) {
                if (rec.getAddress().contains(address.trim())) findRecords.add(rec);
            }
        return findRecords;
    }
}
