package main;

import java.io.*;

public class Main {

    static PhoneBook phoneBook;

    public static void writeToFile() throws IOException {
        FileOutputStream fos = new FileOutputStream("phoneBook.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(phoneBook);
        oos.close();
    }

    public static PhoneBook readFromFile() throws IOException {
        FileInputStream fis = new FileInputStream("phoneBook.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        ois.close();
        return phoneBook;
    }

}
